## @file IMUoperator.py
#  
#
#  
#
#  @author Samara Van Blaricom
#
#  @date May 20, 2020
#
#  
#  @brief A module that employs the I2C class to operate an IMU.
#

class IMUoperator:
    '''An IMU operator object'''
    def __init__(self, bus, dev_address):
        '''Creates the IMUoperator object with the given bus and device address
        @param bus A user input that dictates the bus of the I2C
        @param dev_address The slave device's address'''
        import pyb
        from pyb import I2C
        self.bus = bus
        self.dev_address = dev_address
        
    def enable(self):
        '''Enables the IMU'''
        import pyb

        from pyb import I2C
        # Creates an I2C object used to communicate with the IMU.
        self.i2c = I2C(self.bus, I2C.MASTER)
        # Sets the units for angle and angular velocity to degrees and degrees/second, respectively.
        self.i2c.mem_write(0b00000000, 40, 0x3B)
        print('Enabling IMU')
    
    def disable(self):
        ''' Deinitializes the I2C object.'''
        self.i2c.deinit()
        print('Disabling IMU')
    
    def change_mode(self, mode):
        ''' Changes the mode of the IMU sensor to whatever the user inputs.
        @param mode The user input for the operating mode of the IMU'''
    
        self.i2c.mem_write(mode, self.dev_address, 0x3D)
    
    def check_calib(self):
        ''' Checks to see if each part (system, gyroscope, accelerometer, and magnetometer) is calibrated correctly.'''
        # Reads the byte with the calibration data
        c = self.i2c.mem_read(1, self.dev_address, 0x35)
        # Separates and reads the bits for each calibration status
        sys_cal = (c[0]>>6) & 0b11
        gyr_cal = (c[0]>>4) & 0b11
        acc_cal = (c[0]>>2) & 0b11
        mag_cal = (c[0]>>0) & 0b11

        calib_stat = (sys_cal, gyr_cal, acc_cal, mag_cal)
        print(calib_stat)

        
    def read_eul(self):
        '''Reads the Euler angles of the IMU'''
        import ustruct
        # Reads the 6 bytes representing the LSB and MSB from x, y, and z data
        self.datae = self.i2c.mem_read(6, 40, 0x1A)
        # Unpacks the data into 3 signed integers
        valuese = ustruct.unpack('<hhh',self.datae)   
        gain = (16, 16, 16)
        # Divides the angle tuple by the conversion gain
        degreese = tuple(ele1 // ele2 for ele1, ele2 in zip(valuese, gain))
        print(degreese)
        
    def read_angvel(self):
        '''Reads the angular velocities of the IMU'''
        import ustruct
        # Read 6 bytes representing the LSB and MSB from x, y, and z data
        self.dataa = self.i2c.mem_read(6, 40, 0x14)
        # Unpack the data into 3 signed integers
        valuesa = ustruct.unpack('<hhh',self.dataa)   
        gaina = (16, 16, 16)
        # Divides the angle tuple by the conversion gain
        degreesa = tuple(ele1 // ele2 for ele1, ele2 in zip(valuesa, gaina))
        print(degreesa)
        

if __name__ == '__main__':
    # Test Code
    from pyb import I2C
    ope = IMUoperator(1, 40)
    ope.enable()
    NDOF = 0b00001100
    ope.change_mode(NDOF)