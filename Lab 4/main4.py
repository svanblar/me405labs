## @file main4.py
#  
#
#  
#
#  @author Samara Van Blaricom
#
#  @date May 20, 2020
#
#  
#  @brief A code that repeatedly calls the IMUoperator module to obtain the Euler angles of the IMU.
#



import pyb
from pyb import I2C
import ustruct
import utime
from IMUoperator import IMUoperator
ope = IMUoperator(1, 40)
ope.enable()
NDOF = 0b00001100
ope.change_mode(NDOF)

while True:
    ope.check_calib()
    ope.read_eul()
    utime.sleep(1)
    
    