
import pyb
import ustruct
from pyb import I2C
i2c = I2C(1, I2C.MASTER)


# Setting the Mode to NDOF
i2c.mem_write(0x0C, 40, 0x3D)

#Checking if it's calibrated
c = i2c.mem_read(1, 40, 0x35)
sys_cal = (c[0]>>6) & 0b11
gyr_cal = (c[0]>>4) & 0b11
acc_cal = (c[0]>>2) & 0b11
mag_cal = (c[0]>>0) & 0b11

calib_stat = (sys_cal, gyr_cal, acc_cal, mag_cal)
print(calib_stat)

# Reading the Euler Heading Angles
i2c.mem_write(0b00000110, 40, 0x3B)
print(i2c.mem_read(1, 40, 0x3B))
data = i2c.mem_read(6, 40, 0x1A)     # read six bytes representing the LSB and MSB from x, y, and z data
values = ustruct.unpack('<hhh',data)   # unpack the data into 3 signed integers
print(values)