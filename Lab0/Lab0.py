# -*- coding: utf-8 -*-
''' @Lab0practice.py'''

## Jeon, T (2019) fibonacci_python [Source code]. https://github.com/Teosoft7/fibonacci_python.

def fib(idx) :
    ''' This method calculateds a Fibonacci number corresponding to a 
    specified index. 
    @param idx An integer specifying the index of the desired 
            Fibonacci number ''' 
    if idx == 0:
        return 0
    elif idx == 1 or idx == 2:
        return 1
    elif idx > 2:
        a = 1 # variable for (n - 1)
        b = 1 # variable for (n - 2)
        for _ in range(3, idx + 1):
            c = a + b
            a, b = b, c

        return c
       
idx = int(input('Input the index of the Fibonacci number desired ' ))       
print ('Calculating Fibonacci number at '
           'index n = {:} . '.format(idx))
        

if __name__ == '__main__':
        while (True):
            if (idx >= 0):
                print('The Fibonacci number is ', fib(idx))
            elif (idx == -1):
                print('Thanks for playing!')
                break
            else: 
                print('Invalid input')
            idx = int(input('Input the index of the Fibonacci number desired or -1 to exit '))
    
