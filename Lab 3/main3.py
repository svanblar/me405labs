## @file main3.py
#  
#
#  
#
#  @author Samara Van Blaricom
#
#  @date May 13, 2020
#
#  
#  @brief A code that runs the motor with a closed-loop controller.
#

# Import the needed modules
from motor import MotorDriver
from encoder import EncoderReader
from controller import ProportionController
import pyb
import utime
# Create the pin objects used for interfacing with the motor driver and encoder
pin_EN = pyb.Pin (pyb.Pin.board.PA10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
EN1_A = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
EN1_B = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
tim4 = pyb.Timer(4,prescaler=0,period=0xffff)
EN2_A = None;
EN2_B = None;
tim8 =  None;
    
# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000)
    
# Create a motor object passing in the pins and timer
moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    
# Enable the motor driver
moe.enable()

# Set up the encoder and update it
enc = EncoderReader(EN1_A, EN1_B, tim4, EN2_A, EN2_B, tim8)
enc.update()
    
# Create a Setpoint used for the controller
Setpoint = 1000

# Set up the vectors for position and time
list_position = []
list_time = []

# This while loop sets up a user input that allows the user to input the proportional gain used for the controller or to quit the program.
while True:
    Kp = float(input('Enter the desired Kp value or enter 0 to quit'))
    if Kp == 0:
        break
    elif Kp < 0:
        print('Please enter a positive number')
    else:
        # This code starts the motor and controller with the user input and an arbitrary initial percent duty.
        initialduty = 10
        moe.set_duty(initialduty)
        pcon = ProportionController( Kp, Setpoint)
        # This for loop updates the duty with the output from the controller and saves values for both position and time.
        for x in range(60):
            utime.sleep_ms(10)
                    
            measuredtime = utime.ticks_ms()
                    
            list_time.append(measuredtime)
                    
            measuredvalue = enc.get_position()
                    
            list_position.append(measuredvalue)
                    
            actuatorsig = pcon.update(measuredvalue)

            moe.set_duty(-actuatorsig)
        # This for loop prints the output position and time into 2 columns separated by a comma.
        for value,time in zip(list_position,list_time):
            print(value,',',time)


    
    





