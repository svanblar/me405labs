## @file tempsens.py
#  Brief doc for tempsens.py
#
#
#  @author Samara Van Blaricom
#
#  @date June 12, 2020
#
#  @package tempsens
#  A module that interfaces with a DS18B20 1-Wire thermometer to take temperature readings.
#
#

'''A temperature sensor object'''
class TemperatureSensor:
    
    def __init__(self):
        '''Initializes the temperature sensor by creating the pin object used to operate it'''
            
        #Import all the needed modules, including modules downloaded from https://github.com/dpgeorge/micropython/tree/master/drivers/onewire   
        import utime
        import machine
        import onewire, ds18x20

        
        # the device is on PB10
        dat = machine.Pin('PB10')

        # create the onewire object
        self.ds = ds18x20.DS18X20(onewire.OneWire(dat))

        # scan for devices on the bus
        self.roms = self.ds.scan()
        
    def readtemp(self):
        '''Reads the temperature of the thermometer.'''
        import utime
        import machine
        import onewire, ds18x20
        # Convert the temperature into a readable format
        self.ds.convert_temp()
        utime.sleep_ms(750)
        # Read the converted temperature
        for rom in self.roms:
            t = self.ds.read_temp(rom)
        return t

