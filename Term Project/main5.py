## @file main5.py
#  
#
#  
#
#  @author Samara Van Blaricom
#
#  @date June 12, 2020
#
#  
#  @brief A code that runs the motor with a closed-loop controller.
#

# Import the needed modules
from motor import MotorDriver
from encoder import EncoderReader
from controller import ProportionController
from tempsens import TemperatureSensor
import pyb
import utime
# Create the pin objects used for interfacing with the motor driver and encoder
pin_EN = pyb.Pin (pyb.Pin.board.PA10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
EN1_A = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
EN1_B = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
tim4 = pyb.Timer(4,prescaler=0,period=0xffff)
EN2_A = None;
EN2_B = None;
tim8 =  None;
    
# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000)
    
# Create a motor object passing in the pins and timer
moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    
# Enable the motor driver
moe.enable()

# Set up the encoder and update it
enc = EncoderReader(EN1_A, EN1_B, tim4, EN2_A, EN2_B, tim8)
enc.update()
    
# Create a Setpoint and proportion used for the controller
Setpoint = -500
Kp = .2


# Calls the temperature sensor and updates the temperature
temp = TemperatureSensor()
t = temp.readtemp()

#This While loop prompts the user for a desired temperature
while True:
    desiredT = float(input('Enter the desired temperature in Celsius or enter 0 to quit'))
    if desiredT== 0:
        break
    elif desiredT < 0:
        print('Please enter a positive number')
    else:
        #If the temperature given is positive and nonzero, this while loop will run
        while True:
            # This if/else statement controls whether the code continues to read the temperature, or if it will initiate the motor based on whether
            # the measured temperature is greater than the desired temperature. 
            if t > desiredT:
                t = temp.readtemp()
                print(t)
            else:
                # This code starts the motor and controller with the user input and an arbitrary initial percent duty.
                initialduty = 10
                moe.set_duty(initialduty)
                pcon = ProportionController( Kp, Setpoint)
                # This for loop starts the P controller and continuosly updates it until the desired position of the motor is met.
                for x in range(60):
                    utime.sleep_ms(10)
                            
                    measuredvalue = enc.get_position()
                                
                    actuatorsig = pcon.update(measuredvalue)

                    moe.set_duty(-actuatorsig)
                break






    