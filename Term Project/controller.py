## @file controller.py
#  Brief doc for controller.py
#
#  Detailed doc for controller.py 
#
#  @author Samara Van Blaricom
#
#  @date May 13, 2020
#
#  @package controller
#  A module that creates a general proportional controller.
#
#


class ProportionController:
    ''' A proportion controller object '''
    
    def __init__(self, Kp, Setpoint):
        '''Sets up the proportional controller with the given setpoint and gain.
        @param Kp A user input that defines the proportion of the controller.
        @param Setpoint A user input that defines the input for the controller.'''

        self.Kp = Kp
        self.Setpoint = Setpoint
        
        print('Setting up proportional controller')

        
        
        
    def update(self, measuredvalue):
        '''Runs the control algorithm and returns an actuation value
        @param measuredvalue A measurement from the system used to find the error'''
        
        self.measuredvalue = measuredvalue
        self._error = self.Setpoint - self.measuredvalue
        
        self.actuatorsig = self._error*self.Kp
        
        
        return self.actuatorsig
        
        