## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  The goal of this project was to set up and operate an encoder to measure the position of a motor. 
#
#  @section sec_purp Purpose
#  The purpose of this lab was to allow us to become familiar with encoders and their functionality. 
#
#  @section sec_test Testing
#  This code was tested in the code itself by inputing a function that gave the class input parameters and function callouts.
#  The code was further tested by physically moving the motor in different directions and using the get_position() command multiple times.
#  
#  @section sec_bugs Bugs and Limitations
#  There are certain limitations to this code. The first is that it can only handle two encoders at the moment. Another limitation is that
#  if the motor were to be turned an amount larger than half of 0xffff but didn't actually overflow, it would be corrected unecessarily.
#
#  @section sec_loc Location
#  The location of this code is at 
#
#
#  @section sec_mot Lab 2
#  
#  This module allowed us to run the motor forward or backward. See more information in the \ref Lab 1 package
#
#  @author Samara Van Blaricom
#
#  @date April 25, 2020
#