## @file Lab 2.2.py
#  Brief doc for Lab 2.2.py
#
#  Detailed doc for Lab 2.2.py 
#
#  @author Samara Van Blaricom
#
#  @date May 6, 2020
#
#  @package Lab 2
#  A module that sets up and reads an encoder.
#

class EncoderReader:
    '''This class initiates an encoder and reads its output'''
    
    def __init__(self, ENA1_pin, ENB1_pin, timer1, ENA2_pin, ENB2_pin, timer2 ):
        '''Sets up the encoder with given pins and timer
        @param ENA_pin A pyb.Pin object used as the input for the encoder channel A
        @param ENB_pin A pyb.Pin object used as the input for the encoder channel B
        @param timer A pyb.Timer object set to encoder counting mode''' 

        import pyb
        # Declare the ENA_pin variable with the input
        self._ENA1_pin = ENA1_pin
        # Declare the ENB_pin variable with the input
        self._ENB1_pin = ENB1_pin
        # Declare the timer variable with the input
        self._timer1 = timer1
        # Declare the ENA_pin variable with the input
        self._ENA2_pin = ENA2_pin
        # Declare the ENB_pin variable with the input
        self._ENB2_pin = ENB2_pin
        # Declare the timer variable with the input
        self._timer2 = timer2
        
        # Designate the timer channel and mode
        if self._ENA2_pin == None:
            self.tim1ch1 = self._timer1.channel(1,pyb.Timer.ENC_A, pin=self._ENA1_pin)
            self.tim1ch2 = self._timer1.channel(2,pyb.Timer.ENC_B, pin=self._ENB1_pin)
        else:
            self.tim1ch1 = self._timer1.channel(1,pyb.Timer.ENC_A, pin=self._ENA1_pin)
            self.tim1ch2 = self._timer1.channel(2,pyb.Timer.ENC_B, pin=self._ENB1_pin)
            self.tim2ch1 = self._timer2.channel(1,pyb.Timer.ENC_A, pin=self._ENA2_pin)
            self.tim2ch2 = self._timer2.channel(2,pyb.Timer.ENC_B, pin=self._ENB2_pin)
        
        # Set initial values for the actual position
        self.actualposition1 = 0
        self.actualposition2 = 0
            
        print('Setting up encoder')
        
    def update(self):
        '''This method allows the user to update the current position of the encoder'''
        # Call for the counter function
        if self._ENA2_pin==None:
            self.position1 = self._timer1.counter()
        else:
            self.position1 = self._timer1.counter()
            self.position2 = self._timer2.counter()
        
        print('Encoder position updated')
        
    def get_position(self):
        '''This method allows the user to read the most updated position of the encoder'''
        if self._ENA2_pin==None:
            
            self.get_delta()
            
            self.actualposition1 = self.delta + self.actualposition1
            
            print('Encoder 1 position is', self.actualposition)
        else:
           
            self.get_delta()

            self.actualposition1 = self.delta1 + self.actualposition1
            self.actualposition2 = self.delta2 + self.actualposition2
            print('Encoder 1 position is', self.actualposition1, 'and Encoder 2 position is', self.actualposition2)
            
            
    def set_position(self, init_position1, init_position2):
        '''This method allows the user to set the inital position of the encoder
            @param init_position1 A signed integer holding the initial position for encoder 1
            @param init_position2 A signed integer holding the initial position for encoder 2'''
        if init_position2 == None:
            self.position1 = self._timer1.counter(init_position1)
        else:
            self.position1 = self._timer1.counter(init_position1)
            self.position2 = self._timer2.counter(init_position2)
    
    
    def get_delta(self):
        '''This method displays the difference between the two most recent calls to update()'''
        # This if/else statement makes it so that if you only use one encoder you only see one output.
        if self._ENA2_pin==None:
            # Note the initial position
            self.iposition = self.position1
            # Update the position and note the final position
            self.update()
            self.fposition = self.position1
            # Subtract the initial position from the final position
            self.deltai = self.fposition-self.iposition
            # Correct for overflow or underflow depending on the value of deltai
            if self.deltai>=32767.5: 
                self.delta = self.deltai-65535
            elif self.deltai<=-32767.5:
                self.delta = self.deltai + 65535
            else:
                self.delta = self.deltai
            print('The change in postion is', self.delta)
        # This is a repetition of the code above but for two encoders this time.
        else:
            self.iposition1 = self.position1
            self.iposition2 = self.position2
            self.update()
            self.fposition1 = self.position1
            self.fposition2 = self.position2
            self.deltai1 = self.fposition1-self.iposition1
            self.deltai2 = self.fposition2-self.iposition2
            if self.deltai1>=32767.5:
                self.delta1 = self.deltai1 - 65535
            elif self.deltai1<=-32767.5:
                self.delta1 = self.deltai1 + 65535
            else:
                self.delta1 = self.deltai1
            if self.deltai2>=32767.5:
                self.delta2 = self.deltai2-65535
            elif self.deltai2<=-32767.5:
                self.delta2 = self.deltai2 + 65535
            else:
                self.delta2 = self.deltai2           
            print('The change in position for encoder 1 is', self.delta1, 'and the change in position for encoder 2 is', self.delta2)
            

            
            
        
        
if __name__ == '__main__':
   # Test code for the class
   
   import pyb
        
        
EN1_A = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
EN1_B = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
tim4 = pyb.Timer(4,prescaler=0,period=0xffff)
EN2_A = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN)
EN2_B = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN)
tim8 =  pyb.Timer(8,prescaler=0,period=0xffff)
        
moe = EncoderReader(EN1_A, EN1_B, tim4, EN2_A, EN2_B, tim8)
        
moe.update()



