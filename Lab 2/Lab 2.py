

class EncoderReader:
    '''This class initiates an encoder and reads its output'''
    
    def __init__(self, ENA1_pin, ENB1_pin, timer1, ENA2_pin, ENB2_pin, timer2 ):
        '''Sets up the encoder with given pins and timer
        @param ENA_pin A pyb.Pin object used as the input for the encoder channel A
        @param ENB_pin A pyb.Pin object used as the input for the encoder channel B
        @param timer A pyb.Timer object set to encoder counting mode''' 

        import pyb
        # Declare the ENA_pin variable with the input
        self._ENA1_pin = ENA1_pin
        # Declare the ENB_pin variable with the input
        self._ENB1_pin = ENB1_pin
        # Declare the timer variable with the input
        self._timer1 = timer1
        # Declare the ENA_pin variable with the input
        self._ENA2_pin = ENA2_pin
        # Declare the ENB_pin variable with the input
        self._ENB2_pin = ENB2_pin
        # Declare the timer variable with the input
        self._timer2 = timer2
        
        # Designate the timer channel and mode
        if self._ENA2_pin == None:
            self.tim1ch1 = self._timer1.channel(1,pyb.Timer.ENC_A, pin=self._ENA1_pin)
            self.tim1ch2 = self._timer1.channel(2,pyb.Timer.ENC_B, pin=self._ENB1_pin)
        else:
            self.tim1ch1 = self._timer1.channel(1,pyb.Timer.ENC_A, pin=self._ENA1_pin)
            self.tim1ch2 = self._timer1.channel(2,pyb.Timer.ENC_B, pin=self._ENB1_pin)
            self.tim2ch1 = self._timer2.channel(1,pyb.Timer.ENC_A, pin=self._ENA2_pin)
            self.tim2ch2 = self._timer2.channel(2,pyb.Timer.ENC_B, pin=self._ENB2_pin)
        
        print('Setting up encoder')
        
        
    def update(self):
        '''This method allows the user to update the current position of the encoder'''
        # Call for the counter function
        if self._ENA2_pin==None:
            self.position1 = self._timer1.counter()
        else:
            self.position1 = self._timer1.counter()
            self.position2 = self._timer2.counter()
        
        print('Encoder position updated')
        
    def get_position(self):
        '''This method allows the user to read the most updated position of the encoder'''
        if self._ENA2_pin==None:
            print('Encoder 1 position is', self.position1)
        else:
            print('Encoder 1 position is', self.position1, 'and Encoder 2 position is', self.position2)
            
            
    def set_position(self, init_position1, init_position2):
        '''This method allows the user to set the inital position of the encoder
            @param init_position1 A signed integer holding the initial position for encoder 1
            @param init_position2 A signed integer holding the initial position for encoder 2'''
        if init_position2 == None:
            self.position1 = self._timer1.counter(init_position1)
        else:
            self.position1 = self._timer1.counter(init_position1)
            self.position2 = self._timer2.counter(init_position2)
    
    
    def get_delta(self):
        '''This method displays the difference between the two most recent calls to update()'''
        if self._ENA2_pin==None:
            self.iposition = self.position1
            self.update()
            self.fposition = self.position1
            self.delta = self.fposition-self.iposition
            print('The change in postion is', self.delta)
        else:
            self.iposition1 = self.position1
            self.iposition2 = self.position2
            self.update()
            self.fposition1 = self.position1
            self.fposition2 = self.position2
            self.delta1 = self.fposition1-self.iposition1
            self.delta2 = self.fposition2-self.iposition2
            print('The change in position for encoder 1 is', self.delta1, 'and the change in position for encoder 2 is', self.delta2)
            
            
        
        
if __name__ == '__main__':
    import pyb
        
        
    EN1_A = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
    EN1_B = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
    tim4 = pyb.Timer(4,prescaler=0,period=0xffff)
    EN2_A = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN)
    EN2_B = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN)
    tim8 =  pyb.Timer(8,prescaler=0,period=0xffff)
        
    moe = EncoderReader(EN1_A, EN1_B, tim4, EN2_A, EN2_B, tim8)
        
    moe.update()



