## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  The purpose of this project was to code a motor to run using Python. 
#
#  @section sec_mot Lab 1
#  
#  This module allowed us to run the motor forward or backward. See more information in the \ref Lab 1 package
#
#  @author Samara Van Blaricom
#
#  @date April 25, 2020
#